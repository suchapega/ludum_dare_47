﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using DG.Tweening;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Assertions.Must;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class GroundhogDayGameController : MonoBehaviour
{
    #region Level data
    public List<LevelData> LevelsData;
    public int CurrentLevelIndex;

    private LevelData _currentLevel;
    #endregion

    public UiController UiController;

    public SpriteRenderer FadeScreen;

    #region Game field
    public RectTransform GameMatrixBackgroundRect;
    public GameObject BlockRef;
    public GameObject[,] Blocks;
    #endregion

    public GameObject GroundHog;
    public Vector3 GroundHogDefScale;

    #region GameLoop
    private Coroutine LoopCoroutine;

    public bool IsPlay { get; private set; }

    public float AnimationTime = 0.75f;

    private Tween _groundHogTween;

    private float _step;
    private Vector3 _startPosition;
    #endregion

    void Awake()
    {
        FadeScreen.transform.position = new Vector3(0.0f, 0.0f, -5f);
        GroundHogDefScale = GroundHog.transform.localScale;
    }

    void Start()
    {
        GroundHog.GetComponent<Groundhog>().EndLevelAction += EndLevelAction;

        #region Set GameField rect
        var height = 2 * Camera.main.orthographicSize;
        var width = height * Camera.main.aspect;
        GameMatrixBackgroundRect.sizeDelta = new Vector2(height, height);
        GameMatrixBackgroundRect.transform.position = new Vector3((width - height) * 0.5f, 0.0f, GameMatrixBackgroundRect.transform.position.z);
        #endregion

        LoadLevel();
    }

    public void RunLoop(List<GroundhogOperations> operations)
    {
        IsPlay = true;

        if (LoopCoroutine != null)
            StopCoroutine(LoopCoroutine);
        
        LoopCoroutine = StartCoroutine(AlgorithmLoop(operations));
    }

    private void LoadLevel()
    {
        FadeScreen.DOColor(new Color(FadeScreen.color.r, FadeScreen.color.g, FadeScreen.color.b, 0.0f), 0.5f).SetEase(Ease.InQuad);

        GroundHog.transform.localScale = GroundHogDefScale;

        _currentLevel = LevelsData[CurrentLevelIndex];

        var cellsData = GameMatrixParser.GetCells(_currentLevel);

        UiController.Init(_currentLevel);
        InitGameField(cellsData);

        GroundHog.GetComponent<Groundhog>().Collider.enabled = true;
        GroundHog.transform.localRotation = UnityEngine.Quaternion.Euler(0, 0, 0);
    }

    public void EndLevelAction(EndLevelType endLevelType)
    {
        switch (endLevelType)
        {
            case EndLevelType.Win:

                UiController.Reset();
                _groundHogTween.Kill();

                GroundHog.GetComponent<Groundhog>().Collider.enabled = false;
                //stop loop
                IsPlay = false;
                StopCoroutine(LoopCoroutine);

                //restart
                if (++CurrentLevelIndex >= LevelsData.Count)
                {
                    CurrentLevelIndex = 0;
                }

                FadeScreen.DOColor(new Color(FadeScreen.color.r, FadeScreen.color.g, FadeScreen.color.b, 1.0f), 0.25f)
                    .SetEase(Ease.InQuad)
                    .OnComplete(LoadLevel);
                break;

            case EndLevelType.Fail:

                _groundHogTween.Kill();

                //stop loop
                IsPlay = false;
                StopCoroutine(LoopCoroutine);

                //goto def state
                SetGroundhogStartPosition(_startPosition);
                GroundHog.transform.localRotation = UnityEngine.Quaternion.Euler(0, 0, 0);

                UiController.Refresh();

                break;
        }
    }

    private void InitGameField(CellData[,] cellsData)
    {
        var rows = cellsData.GetLength(0);
        var columns = cellsData.GetLength(1);

        ClearGameField();
        Blocks = new GameObject[rows, columns];

        var matrixSize = GameMatrixBackgroundRect.sizeDelta;
        var cellWidth = matrixSize.x / columns;
        var cellHeight = matrixSize.y / rows;

        var defaultPosition = GameMatrixBackgroundRect.transform.position
                              + new Vector3(-matrixSize.x / 2f, matrixSize.y / 2f)
                              + new Vector3(cellWidth / 2f, -cellHeight / 2f);

        var scaleXFactor = (BlockRef.GetComponent<BlockItem>().Background.transform.localScale.x * cellWidth) / BlockRef.GetComponent<BlockItem>().Background.size.x;
        var scaleYFactor = (BlockRef.GetComponent<BlockItem>().Background.transform.localScale.y * cellHeight) / BlockRef.GetComponent<BlockItem>().Background.size.y;

        for (var i = 0; i < rows; i++)
        {
            for (var j = 0; j < columns; j++)
            {
                var cellData = cellsData[i, j];

                Blocks[i, j] = Instantiate(BlockRef, GameMatrixBackgroundRect.transform);
                Blocks[i, j].transform.position = defaultPosition + new Vector3(j * cellWidth, -i * cellHeight, 0);

                Blocks[i, j].transform.localScale = new Vector3(scaleXFactor, scaleYFactor, Blocks[i, j].transform.localScale.z);

                Blocks[i, j].GetComponent<BlockItem>().Init(cellData);

                if (cellData.CellType == CellType.Start)
                {
                    _startPosition = Blocks[i, j].transform.position;
                    SetGroundhogStartPosition(_startPosition);

                    GroundHog.transform.localScale = new Vector3(GroundHog.transform.localScale.x * scaleXFactor * 0.75f, GroundHog.transform.localScale.y * scaleYFactor * 0.75f, 1.0f);

                    _step = Blocks[i, j].GetComponent<BlockItem>().Block.size.x * scaleXFactor; //todo check
                }
            }
        }
    }

    public void ClearGameField()
    {
        if (Blocks == null)
            return;

        for (var i = 0; i < Blocks.GetLength(0); i++)
        {
            for (var j = 0; j < Blocks.GetLength(1); j++)
            {
                GameObject.Destroy(Blocks[i, j]);
            }
        }
    }

    public void SetGroundhogStartPosition(Vector2 position)
    {
        GroundHog.transform.position = new Vector3(position.x, position.y, transform.position.z);
    }

    public IEnumerator AlgorithmLoop(List<GroundhogOperations> operations)
    {
        while (IsPlay)
        {
            var tempOperations = new Queue<GroundhogOperations>(operations);

            while (tempOperations.Count != 0)
            {
                var operation = tempOperations.Dequeue();

                switch (operation)
                {
                    case GroundhogOperations.Move:
                        _groundHogTween = GroundHog.transform.DOLocalMove(GroundHog.transform.up * _step, AnimationTime).SetRelative().SetEase(Ease.InOutSine);
                        break;
                    case GroundhogOperations.Turn:
                        _groundHogTween = GroundHog.transform.DOLocalRotate(new Vector3(0.0f, 0.0f, -90.0f), AnimationTime).SetRelative().SetEase(Ease.InOutSine); ;
                        break;
                    case GroundhogOperations.Jump:
                        _groundHogTween = GroundHog.transform.DOLocalMove(GroundHog.transform.up * _step * 2, AnimationTime).SetRelative().SetEase(Ease.InOutSine);
                        break;
                }

                yield return new WaitForSeconds(AnimationTime);
            }
        }
    }

    public void OnDestroy()
    {
        StopAllCoroutines();
    }
}