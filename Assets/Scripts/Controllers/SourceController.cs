﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SourceController : MonoBehaviour
{
    private static SourceController _instance;
    public static SourceController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<SourceController>();
            return _instance;
        }
    }

    [SerializeField] private List<Sprite> Blocks;

    [SerializeField] private List<Sprite> ButtonIcons;

    public Sprite GetBlock(CellType cellType)
    {
        switch (cellType)
        {
            case CellType.Grass:
                return RandomBool() ? null : Blocks.FirstOrDefault(block => block.name == cellType.ToString());
            case CellType.Stone:
                var stones = Blocks.FindAll(block => block.name.Contains(cellType.ToString()));
                return RandomElement(stones);
            default:
                return Blocks.FirstOrDefault(block => block.name == cellType.ToString());
        }

    }

    public Sprite GetIcon(GroundhogOperations operations)
    {
        return GetIcon(operations.ToString());
    }

    public Sprite GetIcon(string name)
    {
        return ButtonIcons.FirstOrDefault(block => block.name == name);
    }

    public static bool RandomBool(float threshold = 0.5f) => Random.value < threshold;

    public static T RandomElement<T>(List<T> list) => list[Random.Range(0, list.Count)];
}
