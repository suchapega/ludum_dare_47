﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UiController : MonoBehaviour
{

    private LevelData _currentLevelData;
    private List<OperationBlock> _operations;

    private readonly Color _defaultOperationColor = new Color(0.8745f, 0.8745f, 0.8745f);
    private readonly Color _selectedOperationColor = new Color(0.3137f, 0.4039f, 0.7215f);

    public GroundhogDayGameController MainController;
    public Canvas OperationsCanvas;
    public Button OperationButtonPrefub;
    public Button RemoveOperationButtonPrefub;
    public Button AddOneMoreOperationButton;

    //LaunchButton
    public Button LaunchButton;
    private const string RunText = "RUN";
    private const string StopText = "STOP";

    private const int BtnSize = 40;
    private const int BtnSpacing = 12;

    private const int BtnGap = BtnSize + BtnSpacing;

    public void Init(LevelData levelData)
    {
        Refresh();

        _currentLevelData = levelData;
        _operations = new List<OperationBlock>();

        AddOneMoreOperationButton.transform.position = new Vector3(0, 0, 0);
        AddOneMoreOperationButton.transform.SetParent(OperationsCanvas.transform, false);

        AddOneMoreOperation();
    }

    private void SelectOperation(string id, GroundhogOperations selectedOperation)
    {
        var operation = _operations.First(x => x.ID == id);
        operation.SelectedOperation = selectedOperation;
        var selectedIndex = _currentLevelData.AvailableOperations.FindIndex(x => x == selectedOperation);
        for (var i = 0; i < _currentLevelData.AvailableOperations.Count; i++)
        {
            operation.Buttons[i].image.color = (i == selectedIndex ? _selectedOperationColor : _defaultOperationColor);
        }
    }

    private void RemoveOperation(string id)
    {
        var operationIndex = _operations.FindIndex(x => x.ID == id);
        foreach (var button in _operations[operationIndex].Buttons)
        {
            Destroy(button.gameObject);
        }

        for (var i = operationIndex; i < _operations.Count; i++)
        {
            foreach (var button in _operations[i].Buttons)
            {
                button.transform.localPosition = new Vector3(button.transform.localPosition.x,
                                                             button.transform.localPosition.y + BtnGap,
                                                             button.transform.localPosition.z);
            }
        }
        AddOneMoreOperationButton.transform.localPosition = new Vector3(0, AddOneMoreOperationButton.transform.localPosition.y + BtnGap, 0);
        _operations.RemoveAt(operationIndex);
        AddOneMoreOperationButton.gameObject.SetActive(_operations.Count <= _currentLevelData.OperationsLimit);
    }

    private Button AddOperationButton(int index, int operationIndex, string id)
    {
        var buttonOperation = _currentLevelData.AvailableOperations[operationIndex];
        var button = Instantiate<Button>(OperationButtonPrefub);
        button.transform.position = new Vector3(operationIndex * BtnSize, index * -BtnGap, 0);
        button.transform.SetParent(OperationsCanvas.transform, false);
        button.onClick.AddListener(() =>
        {
            SelectOperation(id, buttonOperation);
        });

        var images = button.GetComponentsInChildren<Image>();
        images[1].sprite = SourceController.Instance.GetIcon(buttonOperation);

        return button;
    }

    public void AddOneMoreOperation()
    {
        var index = _operations.Count;
        var id = System.Guid.NewGuid().ToString();
        var selectedOperation = _currentLevelData.AvailableOperations.First();

        var buttons = new List<Button>();
        var operationIndex = 0;
        foreach (var operation in _currentLevelData.AvailableOperations)
        {
            var operationButton = AddOperationButton(index, operationIndex++, id);
            operationButton.image.color = (operation == selectedOperation ? _selectedOperationColor : _defaultOperationColor);
            buttons.Add(operationButton);
        }

        if (index > 0)
        {
            var removeButton = Instantiate<Button>(RemoveOperationButtonPrefub);
            removeButton.transform.position = new Vector3(operationIndex * BtnSize + BtnSpacing, index * -BtnGap - 5, 0);
            removeButton.transform.SetParent(OperationsCanvas.transform, false);
            removeButton.onClick.AddListener(() =>
            {
                RemoveOperation(id);
            });
            buttons.Add(removeButton);
        }

        _operations.Add(new OperationBlock()
        {
            ID = id,
            Buttons = buttons,
            SelectedOperation = selectedOperation
        });

        AddOneMoreOperationButton.transform.localPosition = new Vector3(0, buttons[0].transform.localPosition.y - BtnGap, 0);
        AddOneMoreOperationButton.gameObject.SetActive(index < _currentLevelData.OperationsLimit - 1);
    }

    private void DisableOperationButtons(bool disable) {
        foreach (var button in OperationsCanvas.GetComponentsInChildren<Button>(true))
        {
            button.interactable = !disable;
            var childImages = button.GetComponentsInChildren<Image>(true);
            if (childImages.Length > 1)
            {
                var iconImage = childImages[1];
                var color = iconImage.color;
                color.a = disable ? 0.3f : 1f;
                iconImage.color = color;
            }
        }
    }

    private bool _isPlay = false;

    //todo improve
    public void RunStop()
    {
        var textRef = LaunchButton.GetComponentInChildren<Text>(true);
        var imageRef = LaunchButton.GetComponentsInChildren<Image>(true)[1];

        _isPlay = !_isPlay;

        if (_isPlay)
        {
            DisableOperationButtons(true);
            MainController.RunLoop(_operations.Select(x => x.SelectedOperation).ToList());
            textRef.text = StopText;
            imageRef.sprite = SourceController.Instance.GetIcon("Stop");
        }
        else
        {
            textRef.text = RunText;
            imageRef.sprite = SourceController.Instance.GetIcon("Run");
            MainController.EndLevelAction(EndLevelType.Fail);
        }
    }
    public void Reset()
    {
        foreach (var operation in _operations)
        {
            foreach (var button in operation.Buttons)
            {
                Destroy(button.gameObject);
            }
        }
    }

    public void Refresh()
    {
        DisableOperationButtons(false);
        _isPlay = false;
        LaunchButton.GetComponentInChildren<Text>(true).text = RunText;
        LaunchButton.GetComponentsInChildren<Image>(true)[1].sprite = SourceController.Instance.GetIcon("Run");
    }
}

public class OperationBlock
{
    public string ID;
    public GroundhogOperations SelectedOperation;
    public List<Button> Buttons;
}