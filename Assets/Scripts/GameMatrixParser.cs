﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class GameMatrixParser
{
    public static CellData[,] GetCells(LevelData levelData)
    {
        var gameRows = levelData.GameField.Split('\n');

        var rowsCount = gameRows.Length;
        var columnsCount = gameRows.First().Split(' ').Length;

        var cells = new CellData[rowsCount, columnsCount];

        for (var row = 0; row < rowsCount; row++)
        {
            var rowItems = gameRows[row].Split(' ');

            for (var column = 0; column < columnsCount; column++)
            {
                var cell = rowItems[column];
                cells[row, column] = new CellData() { CellType = GetCellType(cell) };
            }
        }

        return cells;
    }

    private static CellType GetCellType(string data)
    {
        switch (data)
        {
            case "0":
            default:
                return CellType.Grass;
            case "1":
                return CellType.Stone;
            case "A":
                return CellType.Start;
            case "B":
                return CellType.End;

        }
    }
}