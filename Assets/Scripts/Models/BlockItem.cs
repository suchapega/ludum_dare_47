﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockItem : MonoBehaviour
{
    public SpriteRenderer Block;
    public SpriteRenderer Background;

    public PolygonCollider2D Collider;

    public CellData CellData { get; private set; }

    public void Init(CellData cellData)
    {
        CellData = cellData;

        Block.sprite = SourceController.Instance.GetBlock(CellData.CellType);
        SetCollider();
    }

    private void SetCollider()
    {
        //remove default collider
        Destroy(gameObject.GetComponent<Collider2D>());

        //create custom collider
        gameObject.AddComponent<PolygonCollider2D>();
        Collider = GetComponent<PolygonCollider2D>();
    }
}