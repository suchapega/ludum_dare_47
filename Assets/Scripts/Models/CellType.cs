﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CellType
{
    Grass, //0
    Stone,  //1
    Start, //A
    End,    //B
    Water //W
}
