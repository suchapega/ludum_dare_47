﻿using System;
using DG.Tweening;
using UnityEngine;

public class Groundhog : MonoBehaviour
{
    public Action<EndLevelType> EndLevelAction;

    public Collider2D Collider;

    void OnCollisionEnter2D(Collision2D collision)
    {
        var blockType = collision.gameObject.GetComponent<BlockItem>().CellData.CellType;

        switch (blockType)
        {
            case CellType.Stone:
                EndLevelAction?.Invoke(EndLevelType.Fail);
                break;
            case CellType.End:
                DOVirtual.DelayedCall(0.25f, () => EndLevelAction?.Invoke(EndLevelType.Win));
                
                break;
        }
    }
}
