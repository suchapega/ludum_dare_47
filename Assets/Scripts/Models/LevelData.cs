﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable, CreateAssetMenu(fileName = "LevelData", menuName = "ScriptableObjects/LevelData", order = 1)]
public class LevelData : ScriptableObject
{
    public List<GroundhogOperations> AvailableOperations;

    public int OperationsLimit;

    [TextArea]
    public string GameField;
}
