# Sloopy

*Sloopy* is a sleepy and silly groundhog who know only two simple steps. And you need to help him find his home using cycle algorithm of commands `step` and `turn`.

This game was created on Ludum Dare 47 game jam

Jam Theme: `Stuck in the loop`

## Demo

* You can play here on [itch.io](https://arseniy59.itch.io/sloopy)
* Or watch full trailer on [YouTube](https://youtu.be/wEHhGGNbm7A)

![gif prototype](sloopy.gif)

Created by Suchapega.Games ©